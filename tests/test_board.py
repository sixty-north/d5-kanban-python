from collections import OrderedDict

from hypothesis import assume
from hypothesis import given
from hypothesis.strategies import text, lists, integers, one_of, none, dictionaries
from pytest import raises

from kanban.domain.exceptions import ConstraintError
from kanban.domain.model.board import start_project, WorkLimitError
from kanban.domain.model.entity import DiscardedEntityError
from kanban.domain.model.workitem import register_new_work_item
from utility.itertools import last


@given(name=text(min_size=1),
       description=text(min_size=1))
def test_start_project_creates_a_board(name, description):
    board = start_project(name, description)
    assert board.name == name
    assert board.description == description


@given(description=text(min_size=1))
def test_start_project_with_invalid_name_raises_value_error(description):
    with raises(ValueError):
        start_project("", description)


@given(initial_name=text(min_size=1),
       changed_name=text(min_size=1),
       description=text(min_size=1))
def test_name_setter_changes_name(initial_name, changed_name, description):
    board = start_project(initial_name, description)
    board.name = changed_name
    assert board.name == changed_name
    assert board.description == description


@given(initial_name=text(min_size=1),
       changed_names=lists(text(min_size=1), max_size=100),
       description=text(min_size=1))
def test_name_setter_increments_version(initial_name, changed_names, description):
    board = start_project(initial_name, description)
    initial_version = board.version
    for changed_name in changed_names:
        board.name = changed_name
    final_version = board.version
    assert final_version == initial_version + len(changed_names)


@given(initial_name=text(min_size=1),
       description=text(min_size=1))
def test_name_setter_with_invalid_name_raises_value_error(initial_name, description):
    board = start_project(initial_name, description)
    with raises(ValueError):
        board.name = ''


@given(name=text(min_size=1),
       initial_description=text(min_size=1),
       changed_description=text(min_size=1))
def test_description_setter_changes_description(name, initial_description, changed_description):
    board = start_project(name, initial_description)
    board.description = changed_description
    assert board.name == name
    assert board.description == changed_description


@given(name=text(min_size=1),
       initial_description=text(min_size=1),
       changed_descriptions=lists(text(min_size=1), max_size=100))
def test_description_setter_increments_version(name, initial_description, changed_descriptions):
    board = start_project(name, initial_description)
    initial_version = board.version
    for changed_description in changed_descriptions:
        board.description = changed_description
    final_version = board.version
    assert final_version == initial_version + len(changed_descriptions)


def test_new_board_is_not_discarded():
    board = start_project("Name", "Description")
    assert not board.discarded


def test_discarding_board_marks_it_as_discarded():
    board = start_project("Name", "Description")
    board.discard()
    assert board.discarded


def test_setting_name_of_discarded_board_raises_discarded_entity_error():
    board = start_project("Name", "Description")
    board.discard()
    with raises(DiscardedEntityError):
        board.name = "foo"


def test_setting_description_of_discarded_board_raises_discarded_entity_error():
    board = start_project("Name", "Description")
    board.discard()
    with raises(DiscardedEntityError):
        board.description = "bar"


@given(column_name=text(min_size=1),
       wip_limit=one_of(integers(min_value=1), none()))
def test_add_new_column(column_name, wip_limit):
    board = start_project("Name", "Description")
    board.add_new_column(column_name, wip_limit)
    last_column = last(board.columns())
    assert last_column.name == column_name
    assert last_column.wip_limit == wip_limit


def test_add_new_column_with_empty_column_name_raises_value_error():
    board = start_project("Name", "Description")
    with raises(ValueError):
        board.add_new_column("", 32)


@given(wip_limit=integers(max_value=-1))
def test_add_new_column_with_negative_wip_limit_raises_value_error(wip_limit):
    board = start_project("Name", "Description")
    with raises(ValueError):
        board.add_new_column("valid", wip_limit)


def test_add_new_column_with_duplicate_name_raises_value_error():
    board = start_project("Name", "Description")
    board.add_new_column("first", 10)
    with raises(ValueError):
        board.add_new_column("first", 15)


@given(names_and_wip_limits=dictionaries(
                              keys=text(min_size=1),
                              values=one_of(integers(min_value=1), none()),
                              min_size=0,
                              max_size=10))
def test_adding_multiple_columns_gives_correct_number_of_columns(names_and_wip_limits):
    board = start_project("Name", "Description")
    for name, wip_limit in names_and_wip_limits.items():
        board.add_new_column(name, wip_limit)
    assert sum(1 for _ in board.columns()) == len(names_and_wip_limits)


@given(names_and_wip_limits=dictionaries(
                              keys=text(min_size=1),
                              values=one_of(integers(min_value=1), none()),
                              min_size=0,
                              max_size=10,
                              dict_class=OrderedDict))
def test_adding_multiple_columns_gives_correct_column_attributes(names_and_wip_limits):
    board = start_project("Name", "Description")
    for name, wip_limit in names_and_wip_limits.items():
        board.add_new_column(name, wip_limit)
    assert all((column.name, column.wip_limit) == name_and_wip_limit
               for column, name_and_wip_limit in zip(board.columns(), names_and_wip_limits.items()))


def test_adding_column_to_discarded_board_raises_discarded_entity_error():
    board = start_project("Name", "Description")
    board.discard()
    with raises(DiscardedEntityError):
        board.add_new_column("valid", 42)


def test_adding_column_increments_version():
    board = start_project("Name", "Description")
    initial_version = board.version
    board.add_new_column("valid", 42)
    assert board.version == initial_version + 1


@given(column_name=text(min_size=1),
       wip_limit=one_of(integers(min_value=1), none()))
def test_insert_column_before_first_column(column_name, wip_limit):
    board = start_project("Name", "Description")
    succeeding_column = board.add_new_column("First added", 10)
    board.insert_new_column_before(succeeding_column, column_name, wip_limit)
    first_column = next(board.columns())
    assert first_column.name == column_name
    assert first_column.wip_limit == wip_limit


def test_insert_new_column_with_empty_column_name_raises_value_error():
    board = start_project("Name", "Description")
    succeeding_column = board.add_new_column("First added", 10)
    with raises(ValueError):
        board.insert_new_column_before(succeeding_column, "", 15)


@given(wip_limit=integers(max_value=-1))
def test_insert_new_column_with_negative_wip_limit_raises_value_error(wip_limit):
    board = start_project("Name", "Description")
    succeeding_column = board.add_new_column("First added", 10)
    with raises(ValueError):
        board.insert_new_column_before(succeeding_column, "valid", wip_limit)


def test_insert_new_column_with_duplicate_name_raises_value_error():
    board = start_project("Name", "Description")
    succeeding_column = board.add_new_column("First added", 10)
    with raises(ValueError):
        board.insert_new_column_before(succeeding_column, "First added", 15)


def test_inserting_column_into_discarded_board_raises_discarded_entity_error():
    board = start_project("Name", "Description")
    succeeding_column = board.add_new_column("First added", 10)
    board.discard()
    with raises(DiscardedEntityError):
        board.insert_new_column_before(succeeding_column, "valid", 42)


def test_inserting_a_column_increments_version():
    board = start_project("Name", "Description")
    succeeding_column = board.add_new_column("First added", 10)
    initial_version = board.version
    board.insert_new_column_before(succeeding_column, "Second added", 42)
    assert board.version == initial_version + 1


@given(names_and_wip_limits=dictionaries(
                              keys=text(min_size=1),
                              values=one_of(integers(min_value=1), none()),
                              min_size=1,
                              max_size=10,
                              dict_class=OrderedDict))
def test_inserting_multiple_columns_gives_correct_column_attributes(names_and_wip_limits):
    board = start_project("Name", "Description")
    name_and_wip_limit_iterator = iter(names_and_wip_limits.items())
    succeeding_column = board.add_new_column(*next(name_and_wip_limit_iterator))
    for name, wip_limit in name_and_wip_limit_iterator:
        succeeding_column = board.insert_new_column_before(succeeding_column, name, wip_limit)
    assert all((column.name, column.wip_limit) == name_and_wip_limit
               for column, name_and_wip_limit in zip(board.columns(), reversed(names_and_wip_limits.items())))


@given(names_and_wip_limits=dictionaries(
                              keys=text(min_size=1),
                              values=one_of(integers(min_value=1), none()),
                              min_size=1,
                              max_size=10))
def test_remove_column_by_name(names_and_wip_limits):
    board = start_project("Name", "Description")
    name_to_remove = sorted(names_and_wip_limits.keys())[0]
    for name, wip_limit in names_and_wip_limits.items():
        board.add_new_column(name, wip_limit)
    board.remove_column_by_name(name_to_remove)
    assert all(column.name != name_to_remove for column in board.columns())


@given(names_and_wip_limits=dictionaries(
                              keys=text(min_size=1),
                              values=one_of(integers(min_value=1), none()),
                              min_size=1,
                              max_size=10))
def test_remove_column_by_reference(names_and_wip_limits):
    board = start_project("Name", "Description")
    name_to_remove = sorted(names_and_wip_limits.keys())[0]
    column_to_remove = None
    for name, wip_limit in names_and_wip_limits.items():
        column = board.add_new_column(name, wip_limit)
        if column.name == name_to_remove:
            column_to_remove = column
    board.remove_column(column_to_remove)
    assert all(column.name != name_to_remove for column in board.columns())


def test_remove_non_empty_column_by_name_raises_constraint_error():
    board = start_project("Name", "Description")
    board.add_new_column("First column", None)
    work_item = register_new_work_item("Work Item #1")
    board.schedule_work_item(work_item)
    with raises(ConstraintError):
        board.remove_column_by_name("First column")


def test_remove_non_empty_column_by_reference_raises_constraint_error():
    board = start_project("Name", "Description")
    column = board.add_new_column("First column", None)
    work_item = register_new_work_item("Work Item #1")
    board.schedule_work_item(work_item)
    with raises(ConstraintError):
        board.remove_column(column)


@given(names_and_wip_limits=dictionaries(
                              keys=text(min_size=1),
                              values=one_of(integers(min_value=1), none()),
                              min_size=0,
                              max_size=10),
       name_to_remove=text(min_size=1))
def test_removing_non_existent_column_by_name_raises_value_error(names_and_wip_limits, name_to_remove):
    assume(name_to_remove not in names_and_wip_limits.keys())
    board = start_project("Name", "Description")
    for name, wip_limit in names_and_wip_limits.items():
        board.add_new_column(name, wip_limit)
    with raises(ValueError):
        board.remove_column_by_name(name_to_remove)


def test_removing_a_column_from_a_different_board_raises_value_error():
    board_a = start_project("Board A", "A")
    column_a1 = board_a.add_new_column("Column A1", None)

    board_b = start_project("Board B", "B")
    board_b.add_new_column("Column B1", None)

    with raises(ValueError):
        board_b.remove_column(column_a1)


def test_removing_column_by_name_from_discarded_board_raises_discarded_entity_error():
    board = start_project("Name", "Description")
    board.add_new_column("First added", 42)
    board.discard()
    with raises(DiscardedEntityError):
        board.remove_column_by_name("First added")


def test_removing_column_by_reference_from_discard_board_raises_discarded_entity_error():
    board = start_project("Name", "Description")
    column = board.add_new_column("First added", 42)
    board.discard()
    with raises(DiscardedEntityError):
        board.remove_column(column)


@given(names_and_wip_limits=dictionaries(
                              keys=text(min_size=1),
                              values=one_of(integers(min_value=1), none()),
                              min_size=1,
                              max_size=10))
def test_removing_a_column_by_name_increments_version(names_and_wip_limits):
    board = start_project("Name", "Description")
    name_to_remove = sorted(names_and_wip_limits.keys())[0]
    for name, wip_limit in names_and_wip_limits.items():
        board.add_new_column(name, wip_limit)
    initial_version = board.version
    board.remove_column_by_name(name_to_remove)
    assert board.version == initial_version + 1


@given(names_and_wip_limits=dictionaries(
                              keys=text(min_size=1),
                              values=one_of(integers(min_value=1), none()),
                              min_size=1,
                              max_size=10))
def test_removing_a_column_by_reference_increments_version(names_and_wip_limits):
    board = start_project("Name", "Description")
    name_to_remove = sorted(names_and_wip_limits.keys())[0]
    column_to_remove = None
    for name, wip_limit in names_and_wip_limits.items():
        column = board.add_new_column(name, wip_limit)
        if column.name == name_to_remove:
            column_to_remove = column
    initial_version = board.version
    board.remove_column(column_to_remove)
    assert board.version == initial_version + 1


def test_column_names_is_empty_for_new_board():
    board = start_project("Name", "Description")
    assert sum(1 for _ in board.column_names()) == 0


@given(names_and_wip_limits=dictionaries(
                              keys=text(min_size=1),
                              values=one_of(integers(min_value=1), none()),
                              min_size=1,
                              max_size=10))
def test_find_column_with_name(names_and_wip_limits):
    board = start_project("Name", "Description")
    name_to_find = sorted(names_and_wip_limits.keys())[0]
    column_to_find = None
    for name, wip_limit in names_and_wip_limits.items():
        column = board.add_new_column(name, wip_limit)
        if column.name == name_to_find:
            column_to_find = column
    column_found = board.column_with_name(name_to_find)
    assert column_to_find is column_found


@given(names_and_wip_limits=dictionaries(
                              keys=text(min_size=1),
                              values=one_of(integers(min_value=1), none()),
                              min_size=0,
                              max_size=10),
       name_to_find=text(min_size=1))
def test_finding_non_existent_column_by_name_raises_value_error(names_and_wip_limits, name_to_find):
    assume(name_to_find not in names_and_wip_limits.keys())
    board = start_project("Name", "Description")
    for name, wip_limit in names_and_wip_limits.items():
        board.add_new_column(name, wip_limit)
    with raises(ValueError):
        board.column_with_name(name_to_find)


def test_finding_on_discarded_board_raises_discarded_entity_error():
    board = start_project("Name", "Description")
    board.add_new_column("First", 37)
    board.discard()
    with raises(DiscardedEntityError):
        board.column_with_name("First")


def test_scheduling_work_item_to_a_discarded_board_raises_discarded_entity_error():
    board = start_project("Name", "Description")
    board.add_new_column("First", 37)
    work_item = register_new_work_item("Work Item #1")
    board.discard()
    with raises(DiscardedEntityError):
        board.schedule_work_item(work_item)


def test_scheduling_work_item_to_a_board_with_no_columns_raises_constraint_error():
    board = start_project("Name", "Description")
    work_item = register_new_work_item("Work Item #1")
    with raises(ConstraintError):
        board.schedule_work_item(work_item)


def test_scheduling_the_same_work_item_twice_raises_a_constraint_error():
    board = start_project("Name", "Description")
    board.add_new_column("First", 37)
    work_item = register_new_work_item("Work Item #1")
    board.schedule_work_item(work_item)
    with raises(ConstraintError):
        board.schedule_work_item(work_item)


def test_exceeding_the_wip_limit_when_scheduling_raises_work_limit_error():
    board = start_project("Name", "Description")
    board.add_new_column("First", wip_limit=1)
    work_item_1 = register_new_work_item("Work Item #1")
    work_item_2 = register_new_work_item("Work Item #2")
    board.schedule_work_item(work_item_1)
    with raises(WorkLimitError):
        board.schedule_work_item(work_item_2)


def test_scheduling_a_work_item_makes_it_contained_by_the_board():
    board = start_project("Name", "Description")
    board.add_new_column("First", wip_limit=1)
    work_item_1 = register_new_work_item("Work Item #1")
    board.schedule_work_item(work_item_1)
    assert work_item_1 in board


def test_not_scheduling_a_work_item_makes_it_not_contained_by_the_board():
    board = start_project("Name", "Description")
    board.add_new_column("First", wip_limit=1)
    work_item_1 = register_new_work_item("Work Item #1")
    assert work_item_1 not in board


def test_scheduling_a_work_item_increments_the_board_version():
    board = start_project("Name", "Description")
    board.add_new_column("First", wip_limit=1)
    work_item_1 = register_new_work_item("Work Item #1")
    initial_version = board.version
    board.schedule_work_item(work_item_1)
    assert board.version == initial_version + 1


def test_scheduling_a_work_item_to_a_discarded_board_raises_discarded_entity_error():
    board = start_project("Name", "Description")
    board.add_new_column("First", wip_limit=1)
    work_item_1 = register_new_work_item("Work Item #1")
    board.discard()
    with raises(DiscardedEntityError):
        board.schedule_work_item(work_item_1)


def test_abandon_work_item_removes_work_item_from_board():
    board = start_project("Name", "Description")
    board.add_new_column("First", wip_limit=1)
    work_item_1 = register_new_work_item("Work Item #1")
    board.schedule_work_item(work_item_1)
    board.abandon_work_item(work_item_1)
    assert work_item_1 not in board


def test_abandoning_work_item_not_on_board_raises_value_error():
    board = start_project("Name", "Description")
    board.add_new_column("First", wip_limit=1)
    work_item_1 = register_new_work_item("Work Item #1")
    with raises(ValueError):
        board.abandon_work_item(work_item_1)


def test_abandoning_work_item_increments_the_board_version():
    board = start_project("Name", "Description")
    board.add_new_column("First", wip_limit=1)
    work_item_1 = register_new_work_item("Work Item #1")
    board.schedule_work_item(work_item_1)
    initial_version = board.version
    board.abandon_work_item(work_item_1)
    assert board.version == initial_version + 1


def test_abandoning_work_item_from_a_discarded_board_raises_discarded_entity_error():
    board = start_project("Name", "Description")
    board.add_new_column("First", wip_limit=1)
    work_item_1 = register_new_work_item("Work Item #1")
    board.schedule_work_item(work_item_1)
    board.discard()
    with raises(DiscardedEntityError):
        board.abandon_work_item(work_item_1)


def test_advance_work_item_moves_item_to_next_column():
    board = start_project("Name", "Description")
    first_column = board.add_new_column("First", wip_limit=1)
    second_column = board.add_new_column("Second", wip_limit=1)
    work_item_1 = register_new_work_item("Work Item #1")
    board.schedule_work_item(work_item_1)
    board.advance_work_item(work_item_1)
    assert work_item_1 not in first_column
    assert work_item_1 in second_column


def test_advance_work_item_from_last_column_raises_a_constraint_error():
    board = start_project("Name", "Description")
    first_column = board.add_new_column("First", wip_limit=1)
    second_column = board.add_new_column("Second", wip_limit=1)
    work_item_1 = register_new_work_item("Work Item #1")
    board.schedule_work_item(work_item_1)
    board.advance_work_item(work_item_1)
    with raises(ConstraintError):
        board.advance_work_item(work_item_1)


def test_advancing_work_item_not_on_board_raises_value_error():
    board = start_project("Name", "Description")
    board.add_new_column("First", wip_limit=1)
    board.add_new_column("Second", wip_limit=1)
    work_item_1 = register_new_work_item("Work Item #1")
    with raises(ValueError):
        board.advance_work_item(work_item_1)


def test_advancing_work_item_into_column_exceeding_work_limit_raises_work_limit_error():
    board = start_project("Name", "Description")
    board.add_new_column("First", wip_limit=1)
    board.add_new_column("Second", wip_limit=1)
    work_item_1 = register_new_work_item("Work Item #1")
    board.schedule_work_item(work_item_1)
    board.advance_work_item(work_item_1)
    work_item_2 = register_new_work_item("Work Item #2")
    board.schedule_work_item(work_item_2)
    with raises(WorkLimitError):
        board.advance_work_item(work_item_2)


def test_advancing_work_item_increments_board_version():
    board = start_project("Name", "Description")
    board.add_new_column("First", wip_limit=1)
    board.add_new_column("Second", wip_limit=1)
    work_item_1 = register_new_work_item("Work Item #1")
    board.schedule_work_item(work_item_1)
    initial_version = board.version
    board.advance_work_item(work_item_1)
    assert board.version == initial_version + 1


def test_advancing_work_item_on_a_discarded_board_raises_discarded_entity_error():
    board = start_project("Name", "Description")
    board.add_new_column("First", wip_limit=1)
    board.add_new_column("Second", wip_limit=1)
    work_item_1 = register_new_work_item("Work Item #1")
    board.schedule_work_item(work_item_1)
    board.discard()
    with raises(DiscardedEntityError):
        board.advance_work_item(work_item_1)


@given(names_and_wip_limits=dictionaries(
                              keys=text(min_size=1),
                              values=one_of(integers(min_value=1), none()),
                              min_size=2,
                              max_size=10))
def test_work_item_must_be_advanced_one_less_than_the_number_of_columns_to_be_retired(names_and_wip_limits):
    board = start_project("Name", "Description")
    for name, wip_limit in names_and_wip_limits.items():
        board.add_new_column(name, wip_limit)
    work_item_1 = register_new_work_item("Work Item #1")
    board.schedule_work_item(work_item_1)
    for i in range(1, len(names_and_wip_limits)):
        board.advance_work_item(work_item_1)
    board.retire_work_item(work_item_1)
    assert work_item_1 not in board


@given(names_and_wip_limits=dictionaries(
                              keys=text(min_size=1),
                              values=one_of(integers(min_value=1), none()),
                              min_size=2,
                              max_size=10))
def test_work_items_can_only_be_retired_from_the_last_column(names_and_wip_limits):
    column_to_advance_to = sorted(names_and_wip_limits.keys())[0]
    assume(column_to_advance_to != last(names_and_wip_limits.keys()))
    board = start_project("Name", "Description")
    for name, wip_limit in names_and_wip_limits.items():
        board.add_new_column(name, wip_limit)
    work_item_1 = register_new_work_item("Work Item #1")
    board.schedule_work_item(work_item_1)
    while work_item_1 not in board.column_with_name(column_to_advance_to):
        board.advance_work_item(work_item_1)
    with raises(ConstraintError):
        board.retire_work_item(work_item_1)


def test_retiring_a_work_item_not_on_the_board_raises_a_value_error():
    board = start_project("Name", "Description")
    board.add_new_column("First", 10)
    work_item_1 = register_new_work_item("Work Item #1")
    with raises(ValueError):
        board.retire_work_item(work_item_1)


def test_retiring_a_work_item_increments_the_board_version():
    board = start_project("Name", "Description")
    board.add_new_column("First", 10)
    work_item_1 = register_new_work_item("Work Item #1")
    board.schedule_work_item(work_item_1)
    initial_version = board.version
    board.retire_work_item(work_item_1)
    assert board.version == initial_version + 1


def test_retiring_a_work_item_from_a_discarded_board_raises_a_discarded_entity_error():
    board = start_project("Name", "Description")
    board.add_new_column("First", 10)
    work_item_1 = register_new_work_item("Work Item #1")
    board.schedule_work_item(work_item_1)
    board.discard()
    with raises(DiscardedEntityError):
        board.retire_work_item(work_item_1)


@given(work_item_names=lists(text(min_size=1)))
def test_obtain_number_of_work_items_from_column(work_item_names):
    work_items = [register_new_work_item(name) for name in work_item_names]
    board = start_project("Name", "Description")
    column = board.add_new_column("First column", None)
    for work_item in work_items:
        board.schedule_work_item(work_item)
    assert column.number_of_work_items == len(work_items)


@given(work_item_names=lists(text(min_size=1)))
def test_full_column_cannot_accept_work_items(work_item_names):
    work_items = [register_new_work_item(name) for name in work_item_names]
    board = start_project("Name", "Description")
    column = board.add_new_column("First column", len(work_items))
    for work_item in work_items:
        board.schedule_work_item(work_item)
    assert not column.can_accept_work_item()


@given(work_item_names=lists(text(min_size=1)))
def test_non_full_column_can_accept_work_items(work_item_names):
    work_items = [register_new_work_item(name) for name in work_item_names]
    board = start_project("Name", "Description")
    column = board.add_new_column("First column", len(work_items) + 1)
    for work_item in work_items:
        board.schedule_work_item(work_item)
    assert column.can_accept_work_item()


@given(work_item_names=lists(text(min_size=1)))
def test_obtain_work_item_ids_from_column(work_item_names):
    work_items = [register_new_work_item(name) for name in work_item_names]
    board = start_project("Name", "Description")
    column = board.add_new_column("First column", None)
    for work_item in work_items:
        board.schedule_work_item(work_item)
    assert list(column.work_item_ids()) == [work_item.id for work_item in work_items]