import asyncio
from aiohttp import web, sys

from webapi.model import init_model, close_model
from webapi.config import config
from webapi.routes import setup_routes


def main(argv=None):

    loop = asyncio.get_event_loop()

    app = web.Application(loop=loop)

    app['config'] = config

    app.on_startup.append(init_model)
    app.on_cleanup.append(close_model)

    setup_routes(app)

    web.run_app(app,
                host=config['host'],
                port=config['port'])
    return 0

if __name__ == '__main__':
    sys.argv(main())
