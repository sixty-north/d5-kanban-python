from webapi.views import (
    get_index,
    get_work_items,
    get_work_item,
    post_work_item,
    delete_work_item,
)


def setup_routes(app):
    app.router.add_get('/', get_index, name='get-index')
    app.router.add_get('/work-item', get_work_items, name='get-work-items')
    app.router.add_get('/work-item/{id}', get_work_item, name='get-work-item')
    app.router.add_post('/work-item', post_work_item, name='post-work-item')
    app.router.add_delete('/work-item/{id}', delete_work_item, name='delete-work-item')