import datetime

from aiohttp import web
from aiohttp.web_exceptions import HTTPCreated

from infrastructure.event_sourced_repos.work_item_repository import WorkItemRepository
from infrastructure.unit_of_work import UnitOfWork, ConflictError
from kanban.domain.exceptions import ConstraintError
from kanban.domain.model.workitem import register_new_work_item
from utility.unique_id import UniqueId


def _work_item_url(request, work_item_id):
    return str(request.url.join(request.app.router['get-work-item'].url_for(id=str(work_item_id))))


def _render_json_work_item(request, work_item):
    body = {
        'links': {'self': (_work_item_url(request, work_item.id))},
        'id': str(work_item.id),
        'attributes': {
            'name': work_item.name,
            'due_date': work_item.due_date.isoformat() if work_item.due_date is not None else None,
            'content': work_item.content
        }
    }
    return body

async def get_index(request):
    body = {'work_items': str(request.app.router['get-work-items'].url_for())}
    return web.json_response(body)


async def get_work_items(request):
    with unit_of_work(request) as u:
        work_item_repo = u.using(WorkItemRepository)
        return web.json_response(
            {'work_items': [_work_item_url(request, work_item_id) for work_item_id in work_item_repo.work_item_ids()]}
        )


async def get_work_item(request):
    try:
        work_item_id = UniqueId.from_hex(request.match_info['id'])
    except ValueError as value_error:
        raise web.HTTPBadRequest(text=str(value_error))

    try:
        with unit_of_work(request) as u:
            work_item = u.using(WorkItemRepository).work_item_with_id(work_item_id)
    except ValueError as value_error:
        raise web.HTTPNotFound(text=str(value_error))
    except ConflictError as conflict_error:
        raise web.HTTPConflict(text=str(conflict_error))
    else:
        body = _render_json_work_item(request, work_item)
        return web.json_response(body)


async def post_work_item(request):
    raw_data = await request.json()

    try:
        name = raw_data['name']
        due_date = datetime.datetime.strptime(raw_data['due_date'], '%Y-%m-%d').date() if raw_data['due_date'] else None
        content = raw_data['content']
        work_item = register_new_work_item(name, due_date, content)
    except (KeyError, ValueError) as e:
        raise web.HTTPBadRequest(text=str(e))

    try:
        with unit_of_work(request) as u:
            u.using(WorkItemRepository).put(work_item)
    except ConflictError as e:
        raise web.HTTPConflict(text=str(e))

    headers = {
        'Location': (_work_item_url(request, work_item.id)),
        'Content-Location': (_work_item_url(request, work_item.id))
    }

    body = _render_json_work_item(request, work_item)

    return web.json_response(
        status=HTTPCreated.status_code,
        headers=headers,
        data=body,
    )


async def delete_work_item(request):
    try:
        work_item_id = UniqueId.from_hex(request.match_info['id'])
    except (KeyError, ValueError) as e:
        raise web.HTTPBadRequest(text=str(e))

    try:
        with unit_of_work(request) as u:
            u.using(WorkItemRepository).discard(work_item_id)
    except ValueError as e:
        raise web.HTTPNotFound(text=str(e))
    except ConstraintError as e:
        raise web.HTTPConflict(text=str(e))
    return web.HTTPNoContent()


def unit_of_work(request):
    app = request.app
    return UnitOfWork(app['eq'], app['es'])
