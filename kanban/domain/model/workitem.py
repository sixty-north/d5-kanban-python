from abc import ABCMeta, abstractmethod
from functools import singledispatch
import reprlib

from kanban.domain.model.entity import Entity
from kanban.domain.model.events import publish, DomainEvent
from utility.unique_id import make_unique_id

# ======================================================================================================================
# Aggregate root entity
#


class WorkItem(Entity):

    class Created(Entity.Created):
        pass

    class Discarded(Entity.Discarded):
        pass

    class NameChanged(DomainEvent):
        pass

    class DueDateChanged(DomainEvent):
        pass

    class ContentChanged(DomainEvent):
        pass

    def __init__(self, event):
        """DO NOT CALL DIRECTLY.
        """

        super().__init__(event.aggregate_id, event.entity_version)
        self._name = event.name
        self._due_date = event.due_date
        self._content = event.content

    def __repr__(self):
        return "{d}WorkItem(id={id}, name={name!r}, due_date={date!r}, content={content})".format(
            d="*Discarded* " if self._discarded else "",
            id=self.id,
            name=self._name,
            date=self._due_date,
            content=reprlib.repr(self._content))

    @property
    def name(self):
        self._check_not_discarded()
        return self._name

    @staticmethod
    def _validate_name(name):
        if len(name) < 1:
            raise ValueError("WorkItem name cannot be empty")
        return name

    @name.setter
    def name(self, value):
        self._check_not_discarded()
        event = WorkItem.NameChanged(aggregate_id=self.id,
                                     _aggregate_instance_id = self.instance_id,
                                     entity_id=self.id,
                                     entity_version=self.version,
                                     name=WorkItem._validate_name(value))
        self._apply(event)
        publish(event)

    @property
    def due_date(self):
        """An optional due-date.

        If the work item has no due-date, this property will be None.

        Raises:
            DiscardedEntityError: When getting or setting, if this work item has been discarded.
        """
        self._check_not_discarded()
        return self._due_date

    @due_date.setter
    def due_date(self, value):
        self._check_not_discarded()
        event = WorkItem.DueDateChanged(aggregate_id=self.id,
                                        _aggregate_instance_id = self.instance_id,
                                        entity_id=self.id,
                                        entity_version=self.version,
                                        due_date=value)
        self._apply(event)
        publish(event)

    @property
    def content(self):
        self._check_not_discarded()
        return self._content

    @content.setter
    def content(self, value):
        self._check_not_discarded()
        event = WorkItem.ContentChanged(aggregate_id=self.id,
                                        _aggregate_instance_id = self.instance_id,
                                        entity_id=self.id,
                                        entity_version=self.version,
                                        content=value)
        self._apply(event)
        publish(event)

    def _discard(self):
        """Discard the WorkItem.

        DO NOT CALL DIRECTLY.

        Only work items that have been put into repository can be discarded. Hmmm.
        """
        self._check_not_discarded()
        event = WorkItem.Discarded(aggregate_id=self.id,
                                   _aggregate_instance_id = self.instance_id,
                                   entity_id=self.id,
                                   entity_version=self.version)
        self._apply(event)
        publish(event)

    def _apply(self, event):
        mutate(self, event)


# ======================================================================================================================
# Factories - the aggregate root factory
#

def register_new_work_item(name, due_date=None, content=None):
    """Register a new work item.

    Args:
        name (str): A non-empty string.
        due_date (datetime): The due date.
        content (str): Descriptive text.

    Raises:
        ValueError: For invalid field values.
    """
    work_item_id = make_unique_id()

    event = WorkItem.Created(aggregate_id=work_item_id,
                             entity_id=work_item_id,
                             entity_version=0,
                             name=WorkItem._validate_name(name),
                             due_date=due_date,
                             content=content)

    work_item = _when(event)
    event._aggregate_instance_id = work_item.instance_id
    publish(event)
    return work_item


# ======================================================================================================================
# Mutators - all aggregate creation and mutation is performed by the generic _when() function.
#

def mutate(obj, event):
    return _when(event, obj)


# These dispatch on the type of the first arg, hence (event, self)
@singledispatch
def _when(event, entity):
    """Modify an entity (usually an aggregate root) by replaying an event."""
    raise NotImplementedError("No _when() implementation for {!r} against {!r}".format(event, entity))


@_when.register(WorkItem.Created)
def _(event, unused=None):
    assert unused is None
    work_item = WorkItem(event)
    return work_item


@_when.register(WorkItem.NameChanged)
def _(event, work_item):
    work_item._validate_event_applicability(event)
    work_item._name = event.name
    work_item._increment_version()
    return work_item


@_when.register(WorkItem.DueDateChanged)
def _(event, work_item):
    work_item._validate_event_applicability(event)
    work_item._due_date = event.due_date
    work_item._increment_version()
    return work_item


@_when.register(WorkItem.ContentChanged)
def _(event, work_item):
    work_item._validate_event_applicability(event)
    work_item._content = event.content
    work_item._increment_version()
    return work_item


@_when.register(WorkItem.Discarded)
def _(event, work_item):
    work_item._validate_event_applicability(event)
    work_item._discarded = True
    work_item._increment_version()
    return None

# ======================================================================================================================
# Repository - for retrieving existing aggregates
#

class Repository(metaclass=ABCMeta):

    @abstractmethod
    def put(self, work_item):
        raise NotImplementedError

    @abstractmethod
    def work_item_with_id(self, work_item_id):
        """Retrieve a WorkItem by id.

        Args:
            work_item_id: An UniqueId corresponding to the WorkItem.

        Returns:
            The WorkItem.

        Raises:
            ValueError: If a WorkItem with the id could not be found.
        """
        raise NotImplementedError

    @abstractmethod
    def work_items_with_ids(self, work_item_ids=None):
        """Retrieve multiple WorkItems by id.

        Args:
            work_item_ids: An optional iterable series of UniqueIds to
                which the results will be restricted. If None, all work items
                will be returned.

        Returns:
            An iterable series of WorkItems.
        """
        raise NotImplementedError

    @abstractmethod
    def work_items_with_name(self, name, work_item_ids=None):
        """Retrieve WorkItems by name.

        Args:
            name (str): Work items with names equal to this value will
                be included in the result.

            work_item_ids: An optional iterable series of UniqueIds to
                which the results will be restricted. If None, all work items
                matching name will be returned.

        Returns:
            An iterable series of work items.
        """
        raise NotImplementedError

    @abstractmethod
    def discard(self, work_item_id):
        """Delete a WorkItem permanently.

        Args:
            work_item_id: The ID of the work item to be deleted.

        Raises:
            ValueError: If there is no work item corresponding to the ID.
            ConstraintError: If the WorkItem could not be deleted.
        """
        raise NotImplementedError
