from pprint import pprint as pp
from datetime import date

from infrastructure.event_queue import EventQueue
from infrastructure.event_sourced_projections.board_lead_time_projection import LeadTimeProjection
from infrastructure.event_sourced_repos.board_repository import BoardRepository
from infrastructure.event_sourced_repos.work_item_repository import WorkItemRepository
from infrastructure.event_store import JsonFileStore, EventStore
from infrastructure.event_queue_subscriber import EventQueueSubscriber
from infrastructure.transcoders import ObjectJSONEncoder, ObjectJSONDecoder
from infrastructure.unit_of_work import UnitOfWork

from kanban.domain.model.board import start_project
from kanban.domain.model.workitem import register_new_work_item
from kanban.domain.services.overdue import locate_overdue_work_items
from utility.unique_id import UniqueId

UniqueId.abbreviated_length = 6


def main():
    jfs = JsonFileStore("store.events",
                        json_encoder_class=ObjectJSONEncoder,
                        json_decoder_class=ObjectJSONDecoder)
    es = EventStore(jfs)
    eq = EventQueue()
    eqs = EventQueueSubscriber(eq)

    with UnitOfWork(eq, es) as unit_of_work:

        board_repo = unit_of_work.using(BoardRepository)

        board = start_project("Test", "A test project")

        board_repo.put(board)

        board_id = board.id

        board.name = "Another name"
        board.description = "A different description"

        todo_column = board.add_new_column("To do", 20)
        doing_column = board.add_new_column("Doing", 3)
        done_column = board.add_new_column("Done", None)

        #todo_column = board.column_with_name("To do")
        #impeded_column = board.insert_new_column_before(todo_column, "Impeded", 7)

        #board.remove_column(doing_column)

        print(repr(doing_column))

        work_item_repo = unit_of_work.using(WorkItemRepository)

        work_item_1 = register_new_work_item(name="Feature 1",
                                             due_date=date(2013, 1, 12),
                                             content="Here's some info about how to make feature 1")
        work_item_repo.put(work_item_1)

        work_item_2 = register_new_work_item(name="Feature 2",
                                             due_date=date(2014, 8, 13),
                                             content="Here's some info about how to make feature 2")
        work_item_repo.put(work_item_2)

        work_item_3 = register_new_work_item(name="Feature 3",
                                             due_date=None,
                                             content="Here's some info about how to make feature 3")
        work_item_repo.put(work_item_3)

        work_item_4 = register_new_work_item(name="Feature 4",
                                             due_date=date(2015, 3, 2),
                                             content="Here's some info about how to make feature 3")
        work_item_repo.put(work_item_4)

        work_item_4.due_date = None

        board.schedule_work_item(work_item_3)
        board.schedule_work_item(work_item_1)
        board.schedule_work_item(work_item_2)
        board.schedule_work_item(work_item_4)

        board.abandon_work_item(work_item_2)

        board.advance_work_item(work_item_3)
        board.advance_work_item(work_item_3)
        board.advance_work_item(work_item_4)

        board.retire_work_item(work_item_3)

        overdue_work_items = locate_overdue_work_items(board, work_item_repo)

        pp(list(overdue_work_items))

        boards = board_repo.boards_with_name("Another name")

        #work_item_repo.save_changes()
        #board_repo.save_changes()

    # Note: We require only that projections work on saved changes, not on unsaved, or uncommitted changes.
    #lead_time_projection = LeadTimeProjection(board_id, es)
    #print(lead_time_projection.average_lead_time)

    board.advance_work_item(work_item_1)
    board.advance_work_item(work_item_1)
    board.retire_work_item(work_item_1)
    #print(lead_time_projection.average_lead_time)

    board_again = board_repo.board_with_id(board_id)
    print(board, board_again)
    assert board is board_again

    #lead_time_projection.close()

    # TODO: Note this causes the items to be saved to the event store in a different order from which they
    # TODO: occurred. This is not a huge issue given that each aggregate boundary should have be consistency
    # TODO  boundary, so this reordering shouldn't be important. The data will be *eventually consistent*.

    #work_item_repo.save_changes()
    #board_repo.save_changes()

    eqs.close()
    eq.close()

    #ps.close()


if __name__ == '__main__':
    main()
